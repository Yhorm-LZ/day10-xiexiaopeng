**O(objective)**:

Learn flyway. Flyway is like git, which can help to manage database version when we update database constantly. And learn mapper to map Entity with Response and Request. It can make Entity more focus on data, not bussiness.

Learn about the concept of Cloud Native, including devOps, CI/CD, microservice and containers. devOps is a develop method use to promote communication and cooperation among development, operation and test departments.CI is the process of testing, building after the code is pushed to and from the remote repository, before this commit is merged into the main branch. CD is next to CI by pushing the deployment of the default branch of the warehouse into the production environment. Container provides a consistent and isolated environment for running applications, allowing them to be portable and run consistently across different computing environments. Those are all concept learning. About my personal understanding, I think cloud native is a struct helps us scheduling resource dynamically, and help us deploy automatically and safely.

<br><br>**R(Reflective)**:Good
<br><br>**I(Interpretive)**:Using mapper can make Entity more focus on data, not bussiness. Cloud Native helps us schedule resource dynamically, and help us deploy automatically and safely.
<br><br>**D(Decisional)**:Cloud Native includes a lot of concept I never heard before. I need to learn more about what is it and how to use it.