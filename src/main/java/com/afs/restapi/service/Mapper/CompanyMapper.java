package com.afs.restapi.service.Mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.service.Dto.CompanyRequest;
import com.afs.restapi.service.Dto.CompanyResponse;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    public static Company toEntity(CompanyRequest companyRequest){
        Company company=new Company();
        BeanUtils.copyProperties(companyRequest,company);
        return company;
    }

    public static CompanyResponse toResponse(Company company){
        CompanyResponse companyResponse=new CompanyResponse();
        BeanUtils.copyProperties(company,companyResponse);
        companyResponse.setEmployeeCount(company.getEmployees().size());
        return companyResponse;
    }
}
