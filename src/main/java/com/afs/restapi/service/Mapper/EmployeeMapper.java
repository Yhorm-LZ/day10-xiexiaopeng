package com.afs.restapi.service.Mapper;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.Dto.EmployeeRequest;
import com.afs.restapi.service.Dto.EmployeeResponse;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public static Employee toEntity(EmployeeRequest employeeRequest){
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeRequest,employee);
        return employee;
    }
    public static EmployeeResponse toResponse(Employee employee){
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee,employeeResponse);
        return employeeResponse;
    }
}
