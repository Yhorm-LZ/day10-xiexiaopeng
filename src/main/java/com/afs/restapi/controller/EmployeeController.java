package com.afs.restapi.controller;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.Dto.EmployeeRequest;
import com.afs.restapi.service.Dto.EmployeeResponse;
import com.afs.restapi.service.EmployeeService;
import com.afs.restapi.service.Mapper.EmployeeMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<EmployeeResponse> getAllEmployees() {
        List<EmployeeResponse> response= new ArrayList<>();
        employeeService.findAll().forEach(employee -> response.add(EmployeeMapper.toResponse(employee)));
        return response;
    }

    @GetMapping("/{id}")
    public EmployeeResponse getEmployeeById(@PathVariable Long id) {
        return EmployeeMapper.toResponse(employeeService.findById(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public EmployeeResponse updateEmployee(@PathVariable Long id, @RequestBody EmployeeRequest employeeRequest) {
        Employee employee=EmployeeMapper.toEntity(employeeRequest);
        return EmployeeMapper.toResponse(employeeService.update(id, employee));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable Long id) {
        employeeService.delete(id);
    }

    @GetMapping(params = "gender")
    public List<EmployeeResponse> getEmployeesByGender(@RequestParam String gender) {
        List<EmployeeResponse> response= new ArrayList<>();
        employeeService.findAllByGender(gender).forEach(employee -> response.add(EmployeeMapper.toResponse(employee)));
        return response;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EmployeeResponse createEmployee(@RequestBody EmployeeRequest employeeRequest) {
        Employee employee = employeeService.create(EmployeeMapper.toEntity(employeeRequest));
        return EmployeeMapper.toResponse(employee);
    }

    @GetMapping(params = {"page", "size"})
    public List<EmployeeResponse> findEmployeesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        List<EmployeeResponse> response= new ArrayList<>();
        employeeService.findByPage(page, size).forEach(employee -> response.add(EmployeeMapper.toResponse(employee)));
        return response;
    }

}
