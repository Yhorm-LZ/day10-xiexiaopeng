package com.afs.restapi.controller;

import com.afs.restapi.entity.Company;
import com.afs.restapi.service.CompanyService;
import com.afs.restapi.service.Dto.CompanyRequest;
import com.afs.restapi.service.Dto.CompanyResponse;
import com.afs.restapi.service.Dto.EmployeeResponse;
import com.afs.restapi.service.Mapper.CompanyMapper;
import com.afs.restapi.service.Mapper.EmployeeMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("companies")
@RestController
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public List<CompanyResponse> getAllCompanies() {
        List<CompanyResponse> response=new ArrayList<>();
        companyService.findAll().forEach(company -> {
            response.add(CompanyMapper.toResponse(company));
        });
        return response;
    }

    @GetMapping(params = {"page", "size"})
    public List<CompanyResponse> getCompaniesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        List<CompanyResponse> response=new ArrayList<>();
        companyService.findByPage(page, size).forEach(company -> {
            response.add(CompanyMapper.toResponse(company));
        });
        return response;
    }

    @GetMapping("/{id}")
    public CompanyResponse getCompanyById(@PathVariable Long id) {
        return CompanyMapper.toResponse(companyService.findById(id));
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public CompanyResponse updateCompany(@PathVariable Long id, @RequestBody CompanyRequest companyRequest) {
        Company company=CompanyMapper.toEntity(companyRequest);
        return CompanyMapper.toResponse(companyService.update(id, company));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyService.delete(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyResponse createCompany(@RequestBody CompanyRequest companyRequest) {
        Company company=companyService.create(CompanyMapper.toEntity(companyRequest));
        return CompanyMapper.toResponse(company);
    }

    @GetMapping("/{id}/employees")
    public List<EmployeeResponse> getEmployeesByCompanyId(@PathVariable Long id) {
        List<EmployeeResponse> response= new ArrayList<>();
        companyService.findEmployeesByCompanyId(id).forEach(employee -> response.add(EmployeeMapper.toResponse(employee)));
        return response;
    }

}
