package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.Dto.EmployeeRequest;
import com.afs.restapi.service.Mapper.EmployeeMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CompanyJPARepository companyJPARepository;

    @Autowired
    private EmployeeJPARepository employeeJPARepository;

    @BeforeEach
    void setUp() {
        employeeJPARepository.deleteAll();
        companyJPARepository.deleteAll();
    }

    @Test
    void should_update_employee_age_and_salary() throws Exception {
        Employee previousEmployee = new Employee(1L, "zhangsan", 22, "Male", 1000);
        Employee actualEmployee = employeeJPARepository.save(previousEmployee);

        Employee employeeUpdateRequest = new Employee(1L, "lisi", 24, "Female", 2000);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(employeeUpdateRequest);
        mockMvc.perform(put("/employees/{id}", actualEmployee.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<Employee> optionalEmployee = employeeJPARepository.findById(actualEmployee.getId());
        assertTrue(optionalEmployee.isPresent());
        Employee updatedEmployee = optionalEmployee.get();
        Assertions.assertEquals(employeeUpdateRequest.getAge(), updatedEmployee.getAge());
        Assertions.assertEquals(employeeUpdateRequest.getSalary(), updatedEmployee.getSalary());
        Assertions.assertEquals(actualEmployee.getId(), updatedEmployee.getId());
        Assertions.assertEquals(previousEmployee.getName(), updatedEmployee.getName());
        Assertions.assertEquals(previousEmployee.getGender(), updatedEmployee.getGender());
    }

    @Test
    void should_create_employee() throws Exception {
        EmployeeRequest request = getEmployeeRequestZhangsan();

        ObjectMapper objectMapper = new ObjectMapper();
        String employeeRequest = objectMapper.writeValueAsString(request);
        mockMvc.perform(post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(request.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(request.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(request.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());
    }

    @Test
    void should_find_employees() throws Exception {
        Company company=getCompany1();
        Long companyId = companyJPARepository.save(company).getId();
        Employee employee = getEmployeeZhangsan();
        employee.setCompanyId(companyId);

        Employee actualEmployee = employeeJPARepository.save(employee);
        mockMvc.perform(get("/employees"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(actualEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(actualEmployee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(actualEmployee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(actualEmployee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").value(actualEmployee.getCompanyId()));
    }

    @Test
    void should_find_employee_by_id() throws Exception {
        Employee employee = getEmployeeZhangsan();
        Employee actualEmployee = employeeJPARepository.save(employee);
        mockMvc.perform(get("/employees/{id}", actualEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(actualEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(actualEmployee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(actualEmployee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(actualEmployee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").doesNotExist());
    }

    @Test
    void should_delete_employee_by_id() throws Exception {
        Employee employee = getEmployeeZhangsan();
        Employee actualEmployee = employeeJPARepository.save(employee);

        mockMvc.perform(delete("/employees/{id}", actualEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(employeeJPARepository.findById(1L).isEmpty());
    }

    @Test
    void should_find_employee_by_gender() throws Exception {
        Employee employee = getEmployeeZhangsan();
        Employee actualEmployee = employeeJPARepository.save(employee);

        mockMvc.perform(get("/employees?gender={0}", "Male"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(actualEmployee.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(actualEmployee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(actualEmployee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(actualEmployee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    @Test
    void should_find_employees_by_page() throws Exception {
        Employee employeeZhangsan = getEmployeeZhangsan();
        Employee employeeSusan = getEmployeeSusan();
        Employee employeeLisi = getEmployeeLisi();
        employeeJPARepository.save(employeeZhangsan);
        employeeJPARepository.save(employeeSusan);
        employeeJPARepository.save(employeeLisi);

        mockMvc.perform(get("/employees")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employeeZhangsan.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employeeZhangsan.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employeeZhangsan.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(employeeSusan.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].age").value(employeeSusan.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].gender").value(employeeSusan.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].salary").doesNotExist());
    }

    private static Employee getEmployeeZhangsan() {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        return employee;
    }

    private static EmployeeRequest getEmployeeRequestZhangsan() {
        EmployeeRequest request = new EmployeeRequest();
        request.setName("zhangsan");
        request.setAge(22);
        request.setGender("Male");
        request.setSalary(10000);
        return request;
    }

    private static Employee getEmployeeSusan() {
        Employee employee = new Employee();
        employee.setName("susan");
        employee.setAge(23);
        employee.setGender("Male");
        employee.setSalary(11000);
        return employee;
    }

    private static Employee getEmployeeLisi() {
        Employee employee = new Employee();
        employee.setName("lisi");
        employee.setAge(24);
        employee.setGender("Female");
        employee.setSalary(12000);
        return employee;
    }

    private static Company getCompany1() {
        Company company = new Company();
        company.setName("ABC");
        return company;
    }
}