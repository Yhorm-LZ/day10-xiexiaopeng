package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.service.Dto.CompanyRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CompanyJPARepository companyJPARepository;

    @Autowired
    private EmployeeJPARepository employeeJPARepository;

    @BeforeEach
    void setUp() {
        employeeJPARepository.deleteAll();
        companyJPARepository.deleteAll();
    }

    @Test
    void should_update_company_name() throws Exception {
        Company previousCompany = new Company(1L, "abc");
        Company actualCompany = companyJPARepository.save(previousCompany);

        CompanyRequest companyUpdateRequest = new CompanyRequest();
        companyUpdateRequest.setName("xyz");
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(companyUpdateRequest);
        mockMvc.perform(put("/companies/{id}", actualCompany.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(companyUpdateRequest.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(0));

        Optional<Company> optionalCompany = companyJPARepository.findById(actualCompany.getId());
        assertTrue(optionalCompany.isPresent());
        Company updatedCompany = optionalCompany.get();
        Assertions.assertEquals(actualCompany.getId(), updatedCompany.getId());
        Assertions.assertEquals(companyUpdateRequest.getName(), updatedCompany.getName());
    }

    @Test
    void should_delete_company_name() throws Exception {
        Company company = new Company(1L, "abc");
        Long companyId = companyJPARepository.save(company).getId();
        Employee employee = new Employee(1L, "John",28,"male",5000);
        employee.setCompanyId(companyId);
        Long employeeId = employeeJPARepository.save(employee).getId();

        mockMvc.perform(delete("/companies/{id}", companyId))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(companyJPARepository.findById(companyId).isEmpty());
        assertTrue(employeeJPARepository.findById(employeeId).isEmpty());
    }

    @Test
    void should_create_company() throws Exception {
        Company company = getCompany1();
        CompanyRequest companyRequest=new CompanyRequest();
        companyRequest.setId(company.getId());
        companyRequest.setName(company.getName());
        ObjectMapper objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(companyRequest);
        mockMvc.perform(post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(0));
    }

    @Test
    void should_find_companies() throws Exception {
        Company company = getCompany1();
        Company actrualCompany = companyJPARepository.save(company);
        Employee employee=getEmployee(company);
        employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(actrualCompany.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].employeeCount").value(1));
    }

    @Test
    void should_find_companies_by_page() throws Exception {
        Company company1 = getCompany1();
        Company company2 = getCompany2();
        Company company3 = getCompany3();
        Long id1 = companyJPARepository.save(company1).getId();
        Long id2 = companyJPARepository.save(company2).getId();
        companyJPARepository.save(company3);
        Employee employee=getEmployee(company1);
        employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(id1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].employeeCount").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").value(id2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(company2.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].employeeCount").value(0));
    }

    @Test
    void should_find_company_by_id() throws Exception {
        Company company = getCompany1();
        Long companyId = companyJPARepository.save(company).getId();
        Employee employee = getEmployee(company);
        employeeJPARepository.save(employee);

        mockMvc.perform(get("/companies/{id}", companyId))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(companyId))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(1));
    }

    @Test
    void should_find_employees_by_companies() throws Exception {
        Company company = getCompany1();
        Long companyId = companyJPARepository.save(company).getId();
        Employee employee = getEmployee(company);
        Long employeeId = employeeJPARepository.save(employee).getId();

        mockMvc.perform(get("/companies/{companyId}/employees", companyId))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(employeeId))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").doesNotExist());
    }

    private static Employee getEmployee(Company company) {
        Employee employee = new Employee();
        employee.setName("zhangsan");
        employee.setAge(22);
        employee.setGender("Male");
        employee.setSalary(10000);
        employee.setCompanyId(company.getId());
        return employee;
    }


    private static Company getCompany1() {
        Company company = new Company();
        company.setName("ABC");
        return company;
    }

    private static Company getCompany2() {
        Company company = new Company();
        company.setName("DEF");
        return company;
    }

    private static Company getCompany3() {
        Company company = new Company();
        company.setName("XYZ");
        return company;
    }
}